.PHONY: prepare
prepare:
	npm ci

.PHONY: preview
preview:
	npx hexo server --open

.PHONY: build
build:
	npx hexo clean
	npx hexo generate --bail --force

.PHONY: build_in_podman
build_in_podman:
	bash ./run_in_podman.sh 'make prepare && make build'

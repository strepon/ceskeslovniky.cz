---
title: O slovníku
date: 2019-03-28 11:16:44
---
<span class="tableOfContents">[Ve zkratce](#ve-zkratce) • [Ke stažení](#ke-stazeni) • [Potřebujete něco jiného?](#potrebujete-neco-jineho) • [Prezentace](#prezentace) • [Jak slovník vypadá a jak vznikl](#jak-slovnik-vypada) • [Srovnání úspěšnosti](#srovnani-uspesnosti) • [Jak slovník vylepšit](#jak-slovnik-vylepsit) • [Proč se píše o českých slovnících, když je tu jediný?](#proc-slovniky) • [Kontakt](#kontakt)</span>
<a name="ve-zkratce"></a>
## Ve zkratce

- český slovník kontroly pravopisu s licencí umožňující libovolné využití,

- k vyzkoušení jako rozšíření pro [kancelářský balík LibreOffice](https://extensions.libreoffice.org/extensions/czech-cc0-dictionaries-ceske-cc0-slovniky), aplikace [Firefox](https://addons.mozilla.org/firefox/addon/czech-spellcheck-cc0/), [Thunderbird](https://addons.thunderbird.net/thunderbird/addon/czech-spellcheck-cc0/) nebo [SeaMonkey](https://addons.thunderbird.net/seamonkey/addon/czech-spellcheck-cc0/) a na [zdejším webu](index.html),

- experimentální, sice s obsáhlou slovní zásobou, ale s výraznými systematickými mezerami,

- slova lze snadno doplňovat prostřednictvím Wikidat.

<a name="ke-stazeni"></a>
## Ke stažení

- [rozšíření pro LibreOffice](https://extensions.libreoffice.org/extensions/czech-cc0-dictionaries-ceske-cc0-slovniky),

- [rozšíření pro Firefox](https://addons.mozilla.org/firefox/addon/czech-spellcheck-cc0/),

- [rozšíření pro Thunderbird](https://addons.thunderbird.net/thunderbird/addon/czech-spellcheck-cc0/),

- [rozšíření pro SeaMonkey](https://addons.thunderbird.net/seamonkey/addon/czech-spellcheck-cc0/),

- [repozitář se zdrojovým kódem na GitLabu](https://gitlab.com/strepon/czech-cc0-dictionaries/).

<a name="potrebujete-neco-jineho"></a>
## Potřebujete něco jiného?

- slovník kontroly pravopisu vhodný pro běžné použití → stáhněte si původní rozšíření pro [LibreOffice](https://extensions.libreoffice.org/extensions/czech-dictionaries), [Firefox](https://addons.mozilla.org/firefox/addon/czech-spell-checking-dictionar/), [Thunderbird](https://addons.thunderbird.net/thunderbird/addon/czech-spellcheck-cc0/) nebo [SeaMonkey](https://addons.thunderbird.net/seamonkey/addon/czech-spellcheck-cc0/),

- slovník synonym → stáhněte si [původní rozšíření pro LibreOffice](https://extensions.libreoffice.org/extensions/czech-dictionaries) (slovník generovaný z anglicko-českého slovníku) nebo [rozšíření pro Apache OpenOffice](https://extensions.openoffice.org/en/project/czech-dictionary-pack-ceske-slovniky-cs-cz) (spolehlivější, ale pouze pro nekomerční použití),

- cizojazyčné slovníky → seznamte se se [svobodnými slovníky](https://www.svobodneslovniky.cz/) (zejm. anglicko-český slovník).

<a name="prezentace"></a>
## Prezentace
Podrobnosti o tomto slovníku a o lexémech na Wikidatech se dozvíte také ze záznamů z konferencí [LinuxDays](https://www.youtube.com/watch?v=5LITQ8Ygtzo&list=PLlTxFw5y5UZtIUJgYb-aEcjV21x8TGgLx) (2019), [Wikikonference](https://slideslive.com/38921285/lexemy-na-wikidatech-a-slovnik-pro-kontrolu-pravopisu) (2019) a [OpenAlt](https://www.youtube.com/watch?v=b1kQJs4Y1tg&list=PLOEQDQruWfhyIkXKzxAkEurpVpeI_U-iz) (2020).

<a name="jak-slovnik-vypada"></a>
## Jak slovník vypadá a jak vznikl

Slovník spojuje data ze dvou zdrojů:
- **[Českého tvarotvorného slovníku](https://github.com/plin/slovnik)**, který vytvořili na Masarykově univerzitě v Brně na základě jazykového korpusu; zveřejnili ho v únoru 2019.

- Slovníkových hesel pro český jazyk zadaných do **[Wikidat](https://www.wikidata.org/wiki/Wikidata:Lexicographical_data/cs)**, databáze původně vytvořené pro potřeby Wikipedie. Obsah do Wikidat přidává a spravuje množství dobrovolných přispěvatelů. Slovníková data v nich byla zavedena během roku 2018.

Oba zdroje jsou zveřejněny pod licencí odpovídající licenci [Creative Commons CC0](https://creativecommons.org/publicdomain/zero/1.0/), díky níž je můžeme využívat pro jakékoliv účely.

Drtivá většina slov pochází z Tvarotvorného slovníku, podíl tvarů získaných z Wikidat se pohybuje v řádu procent. Obrovská výhoda Wikidat však spočívá v tom, že do nich můžeme snadno zadávat nová slova, jejich tvary a další informace (kategorie, příznaky apod.). A co víc: odhlédneme-li od tohoto konkrétního slovníku, takto budovaná databáze češtiny, na jednom místě a společnými silami, má potenciál stát se na poli slovníků tím, čím se stala Wikipedie mezi encyklopediemi.

Slovník je nutné považovat za **experimentální**, nevhodný pro běžné nasazení. Přestože Tvarotvorný slovník vzešel z analýzy celé slovní zásoby, zveřejněny byly jen tři slovní druhy: podstatná a přídavná jména a slovesa. Wikidata, kde nalezneme slovní druhy bez omezení, jsou zase výsledkem mravenčí práce jednotlivců a zatím pokrývají jen malou část jazyka. Při zkoušení slovníku si záhy všimnete i dalších systematických nedostatků: chybí stupňovaná přídavná jména a vlastní jména, zahrnuty jsou některé nespisovné tvary a podobně.

Formátem je široce používaný [Hunspell](https://hunspell.github.io/). Zdrojové kódy slovníku jsou k dispozici [na GitLabu](https://gitlab.com/strepon/czech-cc0-dictionaries/), k vyzkoušení je připraveno [rozšíření pro LibreOffice](https://extensions.libreoffice.org/extensions/czech-cc0-dictionaries-ceske-cc0-slovniky) a v repozitáři nalezneme také [doplněk pro software Mozilly](https://gitlab.com/strepon/czech-cc0-dictionaries/-/blob/master/README.md#installation-of-the-mozilla-extension), například Firefox.

![Kontrola pravopisu pomocí rozšíření pro LibreOffice](images/libreoffice_spellcheck.png "Kontrola pravopisu pomocí rozšíření pro LibreOffice")

<a name="srovnani-uspesnosti"></a>
## Srovnání úspěšnosti
Následující tabulka udává pro kombinace několika literárních děl a různých verzí slovníku procentuální podíl slov označených jako nesprávná (buď proto, že nesprávná doopravdy jsou, nebo proto, že ve slovníku schází). Pro srovnání je uveden stejný ukazatel dosažený českým slovníkem kontroly pravopisu s licencí GNU GPL, který je vhodný pro běžné použivání.

||GNU GPL 2018.10|GNU GPL 2021.07|2019.06|2019.08|2019.10|2020.02|2020.04|2020.06|2020.08|2020.10|2020.12|2021.02|2021.04|2021.06|2021.08|2021.10|2021.12|2022.02|2022.04|2022.06|2022.08|2022.10|2022.12|2023.04|2023.08|2023.12|2024.04
|-|-:|-:|-:|-:|-:|-:|-:|-:|-:|-:|-:|-:|-:|-:|-:|-:|-:|-:|-:|-:|-:|-:|-:|-:|-:|-:|-:|
|Dobrodružství Sherlocka Holmese|2,63|1,46|9,02|7,40|7,02|6,56|5,72|5,30|5,01|4,85|4,83|4,80|3,81|3,73|3,73|3,68|3,68|3,68|3,68|3,67|3,67|3,67|3,65|3,64|3,64|3,61|3,62|
|Evangelium podle Jana|0,67|0,41|7,46|5,50|4,91|4,57|3,98|3,45|2,86|2,56|2,46|2,44|2,26|2,17|2,14|1,98|1,98|1,98|1,98|1,98|1,98|1,98|1,98|1,94|1,94|1,94|1,94|
|LibreOffice Writer: Praktický průvodce|3,33|2,95|6,00|5,39|4,76|4,51|4,02|3,89|3,83|3,82|3,81|3,80|3,76|3,74|3,74|3,74|3,74|3,74|3,73|3,72|3,72|3,72|3,69|3,68|3,68|3,68|3,68|
|R.U.R.|8,37|5,11|16,82|12,49|12,03|11,78|10,91|10,79|10,57|10,41|10,39|10,34|10,00|9,95|9,95|9,83|9,83|9,83|9,83|9,83|9,83|9,83|9,82|9,81|9,81|9,81|9,81|
|Ústava České republiky|0,90|0,88|7,89|6,52|6,23|5,55|5,02|4,81|4,72|4,64|4,64|4,64|4,64|4,64|4,64|4,64|4,64|4,64|4,64|4,64|4,64|4,64|4,62|4,62|4,62|4,62|4,62|

<a name="jak-slovnik-vylepsit"></a>
## Jak slovník vylepšit
Slovníku zbývá urazit značný kus cesty k tomu, aby byl spolehlivý – vlastními silami ho ale může zdokonalit každý z nás!

### Chybějící slovo
Scházející slovo snadno přidáte do Wikidat (tam navíc bude užitečné nejen pro tento slovník, ale pro kohokoliv, kdo s Wikidaty pracuje):

1. Ujistěte se, že se slovo na Wikidatech ještě nenachází, např. pomocí
[vyhledávání na stránce Ordia](https://tools.wmflabs.org/ordia/).

2. Vytvořte nové slovo, v terminologii Wikidat „lexém“, pomocí [příslušné stránky](https://www.wikidata.org/wiki/Special:NewLexeme) nebo použijte [přívětivější šablony](https://tools.wmflabs.org/lexeme-forms/) či náš [generátor](generator.html); tyto jsou však dostupné jen pro některé české slovní druhy.

Pokud je potřeba doplnit nové tvary k již existujícímu základnímu tvaru, upravte stránku s lexémem, případně u šablon použijte volbu „Pokročilé“.

K lexémům můžete kromě tvarů doplňovat řadu informací jako významy (s propojením na položku „skutečných“, tj. nikoliv slovníkových Wikidat), výslovnost, dělení slova, příznaky nebo odkazy na slova stejného či opačného významu.

V práci s lexémy vám pomůže [nápověda Wikidat](https://www.wikidata.org/wiki/Wikidata:Lexicographical_data/cs) (převážně anglicky). Budete-li potřebovat radu nebo si chtít něco vyjasnit, využijte diskusi ([obecnou k Wikidatům v češtině](https://www.wikidata.org/wiki/Wikidata:Mezi_bajty) nebo [ke slovníkovým datům v angličtině](https://www.wikidata.org/wiki/Wikidata_talk:Lexicographical_data)).

**Mějte na paměti, že Wikidata mají nejvolnější možnou licenci, proto do nich nelze přebírat slova z jiných slovníků či zdrojů, ty mají v naprosté většině licenci omezenější!** Nelze tedy využít ani data ze slovníku kontroly pravopisu pod licencí GNU GPL, který je aktuálně nejpoužívanějším svobodným slovníkem svého druhu (ale už léta ho nikdo neaktualizuje).

### Nesprávné slovo
Nejdřív zkontrolujte, zda se nesprávné slovo nenachází na Wikidatech, a tam ho můžete případně upravit (například mu přiřadit příznak, že se jedná o nespisovný tvar). Pravděpodobně však bude pocházet z Tvarotvorného slovníku, v tom případě je řešením přidat slovo do seznamu zakázaných slov (blacklistu); to už vyžaduje práci s repozitářem.

### Další možnosti
Obohatit Wikidata a vylepšit slovník můžeme i jinak. Také tyto činnosti vyžadují pokročilejší znalosti:

- zadávání nových a úpravu stávajících lexémů by usnadnily nové [šablony](https://tools.wmflabs.org/lexeme-forms/) pro češtinu, schází namátkou šablona pro nedokonavá slovesa,

- hunspellový slovník je aktuálně tvořen prostým seznamem všech tvarů, přidání pravidel (vzorů) by nejen zmenšilo výsledný soubor, ale především by se tím do slovníku dostala informace o skloňování a časování,

- některé lexémy by bylo možné vytvořit z položek Wikidat (například vlastní jména českých obcí),

- díky stejné licenci lze do Wikidat vkládat data z Tvarotvorného slovníku, kvůli nespisovným a hovorovým tvarům by však import nemohl být plně automatický,

- z [Wikislovníku](https://cs.wiktionary.org/wiki/Wikislovn%C3%ADk:Hlavn%C3%AD_strana), jiného projektu se stejným obsahem jako slovníková data na Wikidatech, kvůli odlišné licenci data importovat nelze, pokud s tím nesouhlasí autoři – [někteří však tak již učinili](https://cs.wiktionary.org/wiki/Wikislovn%C3%ADk:Pod_l%C3%ADpou/Archiv-2018#V%C3%BDznamy_na_Wikidatech_a_licence).

<a name="proc-slovniky"></a>
## Proč se píše o českých slovnících, když je tu jediný?

U slov ve Wikidatech, odkud slovník přebírá data, lze uvádět různorodé informace. Ačkoliv jich tam je v současnosti poskrovnu, jednou z nich snad budeme moci vytvořit nejen slovník kontroly pravopisu, ale třeba také slovník dělení slov, synonym, antonym či výkladový slovník.

<a name="kontakt"></a>
## Kontakt

O slovník se stará tým, který lokalizuje do češtiny kancelářský balík LibreOffice. Napsat nám můžete do [e-mailové konference](https://listarchives.libreoffice.org/cz/lokalizace/) na adresu lokalizace@cz.libreoffice.org (e-mail bude veřejně viditelný).

# ČeskéSlovníky.cz

Repositář pro web ceskeslovniky.cz. Web používá generátor statických stránek [Hexo](https://hexo.io/).

## Úpravy obsahu

### Šablona
Soubory tvořící šablonu HTML se nachází v adresáři `themes`.

### Obsah
Obsah všech stránek je v adresáři `source` a je psaný v Markdownu. Jako jeho dobrý přehled poslouží články:
- [Mastering Markdown - GitHub Guides](https://guides.github.com/features/mastering-markdown/)
- [Basic writing and formatting syntax - User Documentation](https://help.github.com/articles/basic-writing-and-formatting-syntax/)
- [Syntax | kramdown](https://kramdown.gettalong.org/syntax.html)

### Příprava
Abyste byli schopni spustit níže uvedené příkazy, je nutné mít nainstalované [NodeJS](https://nodejs.org/en/download/) ve verzi 12 (nebo novější).

Před prvním sestavením je potřeba stáhnout potřebné závislosti.
```
$ make prepare
```

V případě změny seznamu závislostí v souboru `package.json` je nainstalujte pomocí
```
$ npm install
```

### Náhled
Při úpravách vzhledu i obsahu je dobré rovnou se podívat na výsledek. Níže uvedený příkaz sestaví obsah repositáře, zpřístupní ho na lokální adrese http://localhost:4000/ a otevře ji ve vašem prohlížeči.
```
$ make preview
```
Příkaz stačí spustit jednou v samostatném terminálu a nechat běžet. Pokud pak ve zdrojových souborech provedete nějakou změnu, Hexo sestaví stránky znovu. Pro zobrazení efektu změn stačí obnovit načtenou stránku v prohlížeči (*F5*).

## Sestavení statické verze
Pro sestavení webu slouží tento příkaz.
```
$ make build
```
Statická verze stránek je vygenerovaná do adresáře `public`. Pro nasazení stačí jeho obsah nahrát na server třeba přes FTP.

### Sestavení v kontejneru
Pokud si nechcete instalovat NodeJS na vlastní počítač, můžete použít [`podman`](https://podman.io/) a statickou verzi sestavit v kontejneru spuštěním
```
$ make build_in_podman
```
